using UnrealBuildTool;

public class AA2_BalasteguiTarget : TargetRules
{
	public AA2_BalasteguiTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("AA2_Balastegui");
	}
}
